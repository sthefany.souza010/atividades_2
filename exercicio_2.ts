// A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente a um trabalho de laboratório, a uma avaliação semestral e a um exame final.Faça um programa que receba as três notas, calcule e mostre a média ponderada e o conceito
namespace exercicio_2{

    let nota1, nota2, nota3, peso1, peso2, peso3: number;
    nota1 = 1;
    nota2 = 1,5;
    nota3 = 3;
    peso1 = 2 
    peso2 = 3;
    peso3 = 5;

    let mediaPonderada: number;

    mediaPonderada = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

    if (mediaPonderada >=8 && mediaPonderada <=10)
    {
        console.log("A");
    }
    if (mediaPonderada >=7 && mediaPonderada <=8)
    {
        console.log("B")
    }
    if (mediaPonderada >=6 && mediaPonderada <=7)
    {
        console.log("C")
    }
    if (mediaPonderada >=5 && mediaPonderada <=6)
    {
        console.log("D")
    }
    if (mediaPonderada >=0 && mediaPonderada <=5)
    {
        console.log("E")
        
    }
}