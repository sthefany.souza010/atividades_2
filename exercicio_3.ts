// Faça um programa que receba três números obrigatoriamente em ordem crescente e um quarto número que não siga esta regra. Mostre, em seguida, os quatro números em ordem decrescente.

namespace exercicio_3 {
    let numero1, numero2, numero3, numero4: number;
    numero1 = 2;
    numero2 = 3;
    numero3 = 4;
    numero4 = 1;

    let ordemDecrescene: number;

    if ( numero3 >= 4)
    {
        console.log("4");
    }

    if (numero2 >= 3)
    {
        console.log("3");
    }

    if (numero1 >= 2)
    {
        console.log("2");
    }
    
    if(numero4 <= 1)
    {
        console.log("1");
    }
}