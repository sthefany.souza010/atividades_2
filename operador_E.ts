//Testando o operador E - &&
namespace operador_E{

let idade = 16;
let maiorIdade = idade > 18;
let possuiCarteiraDeMotorista = false;

let podeDirigir = maiorIdade && possuiCarteiraDeMotorista;
console.log(podeDirigir); // false 
}