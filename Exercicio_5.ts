//Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada de acordo com o horário do dia (bom dia, boa tarde ou boa noite).
namespace Exercicio_5
{
    const data = new Date();
    const hora = data.getHours();

    const nome = "Sthefany";

    if(hora >= 6 && hora < 12)
    {
        console.log("Bom dia! " + nome);

    } else if (hora >= 12 && hora < 18)
    {
        console.log("Boa tarde! " + nome);
    }else {
        console.log("Boa noite! " + nome);
    }
    }